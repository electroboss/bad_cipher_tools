import os, math, json
os.system("echo $PYTHONPATH")
print("^ Should have citools/src on the end")
from thing import *

dictionaryf = open("words_dictionary.json")
dictionary = list(json.load(dictionaryf).keys())

def get_combos(letters):
  combos = list(ct.alpha)
  for _ in range(letters-1):
    combos2=ct.dc(combos)
    combos=[]
    for i,x in enumerate(combos2):
      for y in ct.alpha:
        combos.append(x+y)
  return combos

words = [
  "the", "be", "to", "of", "and", "a", "in", "that", "have", "i", "it", "for", "not", "on", "with", "he", "as", "you", "do", "at", "this", "but", "his", "by", "from", "they", "we", "say", "her", "she", "or", "an", "will", "my", "one", "all", "would", "there", "their", "what", "so", "up", "out", "if", "about", "who", "get", "which", "go", "me", "when", "make", "can", "like", "time", "no", "just", "him", "know", "take", "people", "into", "year", "your", "good", "some", "could", "them", "see", "other"
]

bad_sequences = [
  "qa","qb","qc","qd","qe","qf","qg","qh","qi","qj","qk","ql","qm","qn","qo","qp","qr","qs","qt","qv","qw","qx","qy","qz"
]
bad_doubles = ["a","h","i","j","q","u","v","w","x","y"]

def all_possible_arrangements(letters: int, printrec=False):
  if letters == 1:
    return [[0]]
  if printrec:
    print(letters)
  prevarrangements = all_possible_arrangements(letters-1,printrec=printrec)
  arrangements = []
  for i in range(letters):
    for y in prevarrangements:
      y = [k+1 if k>=i else k for k in y]
      arrangements.append(y+[i])
  return arrangements

def all_possible_arrangements_yield(letters):
  print("all_possible_arrangements_yield",letters)
  if letters == 1:
    return [[0]]
  prevarrangements = all_possible_arrangements(letters-1,printrec=True)
  for i in range(letters):
    print(i)
    for y in prevarrangements:
      y = [k+1 if k>=i else k for k in y]
      yield y+[i]

def alltransposition(code, letters, threshold=0, needed="", quiet=False, doallouts=False, printskips=1000):
  strlenargs = str(math.factorial(letters))
  selectouts = []
  if doallouts:
    allouts = []
  try:
    i = -1
    for arrangement in all_possible_arrangements_yield(letters):
      i += 1
      if i%printskips == 0 and quiet==False:
        print(str(i)+"/"+strlenargs)

      out = ct.transposition(code, arrangement)
      if doallouts:
        allouts.append([arrangement,out])
      # check one - needed word in out
      if needed != "":
        if not (needed in out):
          continue
      # check two - 3 of any letter in a row
      q=False
      for letter in ct.alpha:
        if letter*3 in out:
          q=True
          break
      for letter in bad_doubles:
        if letter*2 in out:
          q=True
          break
      for seq in bad_sequences:
        if seq in out:
          q=True
          break
      if q:
        continue
      # check three - the slow one - count the number of words and see if is above threshold
      if threshold != 0:
        numwords = 0
        for word in words:
          if word in out:
            numwords += 1
        if numwords < threshold:
          continue
        
      selectouts.append([arrangement,out])
      if quiet==False and doallouts==False:
        print("yay",i)
  except KeyboardInterrupt:
    print("Keyboard interrupt. Returning.")
  finally:
    if doallouts:
      return selectouts,allouts
    return selectouts


def reselect(allout, threshold, words=words):
  finout = []
  i = 0
  for out in allout:
    wordsinout = 0
    for word in words:
      if word in out[1]:
        wordsinout += 1
    if wordsinout >= threshold:
      finout.append(out)
    if i%10000==0:
      print(i)
    i += 1
  return finout

def alltranstoouts(code, needed=None):
  for i in range(15):
    outf = open("outs/out"+str(i+1)+".txt","w")
    alltrans = alltransposition(code, i+1, 0, quiet=False, needed=needed)[0]
    if len(alltrans) > 0:
      print("Yay "+str(alltrans))
    for x in alltrans:
      outf.write(str(x[0])+" "+x[1]+"\n")
    outf.close()
    print(i+1)

def norep(outs):
  for letter in ct.alpha:
    outs = [out for out in outs if not letter*3 in out]
  return outs

def thingmabobedy(a=1,b=7):
  text = code2[0:20]
  for i in range(a,1+b):
    for j in range(20-i):
      transes = alltransposition(text[j:i+j],i,0, quiet=True)[0]
      for trans in transes:
        if trans[1] in dictionary:
          print("AHA!",trans,i)
      print(j)
    print(i)

def removebadseq(outs):
  newouts = []
  for out in outs:
    q = False
    for letter in bad_doubles:
      if letter*2 in out[1]:
        q=True
        break
    if not q:
      for seq in bad_sequences:
        if seq in out[1]:
          q=True
          break
    if not q:
      newouts.append(out)
  return newouts

def vigenere_freqanalyse(code, kwlen):
  return {x:ct.freqanalyse([code[y*kwlen+x] for y in range(len(code)//kwlen)],1) for x in range(kwlen)}

def columnify(code: str, column_height: str) -> str:
  out = ["@"]*len(code)
  x = 0
  for i in range(len(code)//column_height):
    for j in range(column_height):
      out[j*(len(code)//column_height)+i] = code[x]
      x += 1

  return "".join(out)

def instances(code,samplelen=1):
  instances = dict()
  for i in range(len(code)-samplelen):
    if code[i:i+samplelen] not in instances.keys():
      instances[code[i:i+samplelen]] = [code.count(code[i:i+samplelen]),[i]]
    else:
      instances[code[i:i+samplelen]][1].append(i)
  return instances

# https://www.programiz.com/python-programming/examples/hcf
def hcf(x, y):
   while(y):
       x, y = y, x % y
   return x
