from thing import *
from banane import *
import json

dictionaryf = open("words_dictionary.json")
dictionary = list(json.load(dictionaryf).keys())

def checkwords(code,threshold,keylen,dictionary):
  selectouts=[]
  alldict=dictionary
  dictionary = [x for x in dictionary if len(x) == keylen]
  outs=[]
  i = 0
  lendict = str(len(dictionary))
  for keyword in dictionary:
    out = ct.transposition(code2,keyword)
    outs.append([keyword,out])
    y = 0
    for letter in ct.alpha:
      if letter*3 in out:
        continue
    if threshold != 0:
      for word in alldict:
        if word in out:
          y+=1
      if y>=threshold:
        selectouts.append([keyword,out])
        print("YAY "+str(i)+" "+keyword)
    else:
      selectouts.append([keyword,out])
    i += 1
    if i%1000 == 0:
      print(str(i)+"/"+lendict)
  return selectouts, outs
