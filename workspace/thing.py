import citools as ct

codef = open("input.txt","r")
samplef = open("../previous_answers/all.txt","r")
code = codef.read().replace(" ","").lower()
code1,code2 = code.split("\n")[:2]
sample = samplef.read().lower()
alpha = ct.alpha

def kasiski_examine(code,samplelen):
  instances = dict()
  for i in range(len(code)-samplelen):
    if code.count(code[i:i+samplelen]) > 2:
      if code[i:i+samplelen] not in instances.keys():
        instances[code[i:i+samplelen]] = [code.count(code[i:i+samplelen]),[i]]
      else:
        instances[code[i:i+samplelen]][1].append(i)
  return instances

def kasiski_process(code,samplelen):
  instances = kasiski_examine(code, samplelen)
  for i in instances.keys():
    deltas = []
    for j in range(len(instances[i][1])-1):
      deltas.append(instances[i][1][j+1]-instances[i][1][j])
    instances[i][1] = deltas
  return instances

def gesammtkunswerk(x,code=code2):
  codes=[""]*x
  for i in range(len(code)//x):
    for j in range(x):
      codes[j]+=code[i*x+j]
  return codes

def spaghet(x,y,code=code2):
  if y == -1:
    return [ct.freqanalyse(a, 1) for a in gesammtkunswerk(x,code)]
  return [list(ct.freqanalyse(a, 1).keys())[y] for a in gesammtkunswerk(x,code)]

def ioc(k,code=code2):
  freqs = spaghet(k,-1,code)
  n = len(code)//k
  return [sum([(freqs[y][x]*(freqs[y][x]-1))/(n*(n-1)) for x in ct.alpha]) for y in range(k)]
