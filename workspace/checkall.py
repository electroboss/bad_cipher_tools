from thing import *
from banane import words
import json

dictionaryf = open("words_dictionary.json")
dictionary = list(json.load(dictionaryf).keys())

def checkwords(threshold):
  selectouts=[]
  outs=[]
  i = 0
  lendict = str(len(dictionary))
  for keyword in dictionary:
    out = ct.transposition(code2, keyword)
    outs.append([keyword,out])
    y = 0
    for letter in ct.alpha:
      if letter*3 in out:
        continue
    for word in words:
      if word in out:
        y+=1
    if y>=threshold:
      selectouts.append([keyword,out])
      print("YAY "+str(i)+" "+keyword)
    i += 1
    if i%1000 == 0:
      print(str(i)+"/"+lendict)
  return selectouts, outs