section .text
incpermarr:
  xor al,al
.loop:
  inc byte [edx+eax]
  cmp al, byte [edx+eax]
  jge .end
  mov byte [edx+eax],0
  inc al
  jmp .loop
.end:
  ret

arrtoperm: ; dl is PERMLEN, esi PERMARR, edi PERM
  xor al, al
.loop:
  mov bl, byte [esi+eax]
  mov cl, dl
  sub cl, 2
.l2:
  cmp cl,bl
  jl .endl2
  mov dh, byte [edi+ecx]
  mov byte [edi+ecx+1], dh
  xor dh,dh
  jmp .l2
.endl2:
  mov byte [edi+ebx],al
  inc al
  cmp al,dl
  jl .loop
  ret
