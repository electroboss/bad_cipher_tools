import ms,json

inputf = open("resources/input.txt","r")
outf = open("out.txt","w+")
dictionaryf = open("resources/words_dictionary.json","r")

dictionary = json.load(dictionaryf)
dictionaryf.close()
code = inputf.read()
inputf.close()

wordlist = list(dictionary.keys())

for word in wordlist:
  cipher = ms.kwcipher(word)
  if cipher != None:
    outf.write(ms.substitute(cipher, code)+"\n")

outf.close()