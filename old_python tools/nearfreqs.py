import ms
from random import randint as ri

inputf = open("input.txt","r")
outf = open("outs/out0.txt","w+")
outfno = 0

code = inputf.read()
inputf.close()

freqcipher = ms.freqanalyse(code)[1]
print(freqcipher)

ciphers = []
count = 0

try:
  while 1:
    cipher = list(freqcipher)

    for i in range(25):
      if ri(0,1)==1:
        a=cipher[i]
        cipher[i]=cipher[i+1]
        cipher[i+1]=a

    cipher=[ms.alpha,"".join(cipher)]
    if count%100 == 0:
      print(cipher[1])
    if cipher[1] in ciphers:
      print("Skipping")
      continue
    ciphers.append(cipher[1])
    if count%100 == 0:
      print(len(ciphers))
    outf.write(cipher[1]+" "+ms.substitute(cipher, code))
    count += 1
    if count >= 100000:
      print("Closing outf")
      outf.close()
      outfno += 1
      print("Opening outf"+str(outfno)+".txt")
      outf = open("outs/outf"+str(outfno)+".txt","w+")
      count = 0
except KeyboardInterrupt:
  print("Keyboard interrupt")
finally:
  outf.close()