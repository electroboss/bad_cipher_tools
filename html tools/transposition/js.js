var inputarea = document.getElementById("code");
var outputarea = document.getElementById("output");
var keywordinput = document.getElementById("keyword")
var lettersornumbersinput = document.getElementById("lettersornumbers");
var alpha = "abcdefghijklmnopqrstuvwxyz";

a = function() {
  let arrangement = [];
  if (lettersornumbersinput.checked) {
    arrangement = keywordinput.value.split(" ");
    for (let i=0; i<arrangement.length; i++) {
      arrangement[i] = Number(arrangement[i]);
    }
  } else {
    for (let i=0; i<keywordinput.value.length; i++) {
      arrangement.push(i);
    }
    arrangement.sort(compareFunction=((a,b) => {
      return alpha.indexOf(keywordinput.value[a])-alpha.indexOf(keywordinput.value[b]);
    }))
  }

  let outstr = ""
  var outchunk = [];
  for (let i=0; i<arrangement.length; i++) {
    outchunk.push(0);
  }
  for (let i=0; i<inputarea.value.length/arrangement.length; i++) {
    var chunk = inputarea.value.split("").splice(i*arrangement.length,arrangement.length);
    for (let j=0; j<arrangement.length; j++) {
      outchunk[arrangement.indexOf(j)] = chunk[j];
    }
    outstr += outchunk.join("");
  }

  outputarea.value=outstr;

  window.requestAnimationFrame(a);
}

a();