import sys, pickle
sys.path.append('../python')
import ms

letters = 2
text = open("all.txt","r").read()

chars_to_remove = "\n ,.‘'?()"
text = text.lower()
for char in chars_to_remove:
  text=text.replace(char,"")

while letters >= 1:
  out = open("freqs"+letters+".dat","wb+")

  freqchart = ms.get_freqs(text,letters=letters)
  pickle.dump(freqchart,out)
  letters -= 1