#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFERLEN 3000
#define WORDSLEN 70
#define THRESHOLD 60
#ifndef KEYWORDLEN
#define KEYWORDLEN 5
#define KEYWORD "aaaaa"
#endif
#ifndef OUTPUTFILE
#define OUTPUTFILE "output.txt"
#endif

char words[WORDSLEN][10] = {
  "the", "be", "to", "of", "and", "a", "in", "that", "have", "i", "it", "for", "not", "on", "with", "he", "as", "you", "do", "at", "this", "but", "his", "by", "from", "they", "we", "say", "her", "she", "or", "an", "will", "my", "one", "all", "would", "there", "their", "what", "so", "up", "out", "if", "about", "who", "get", "which", "go", "me", "when", "make", "can", "like", "time", "no", "just", "him", "know", "take", "people", "into", "year", "your", "good", "some", "could", "them", "see", "other"
};

char keyword[] = KEYWORD;
unsigned int inputlen = 0;

int main();
void vigenere(char input[], char keyword[], char output[], unsigned int inputlen, unsigned char keywordlen);
void inckeyword(char keyword[], unsigned char keywordlen);

int main(){
  FILE *fp;
  FILE *outfp;
  char input[BUFFERLEN];
  char output[BUFFERLEN];
  char ch;
  
  fp = fopen("input.txt", "r");
  outfp = fopen(OUTPUTFILE,"w");

  if (fp == NULL) {
    perror("Couldn't find input.txt.\n");
    exit(EXIT_FAILURE);
  }

  unsigned int i = 0;
  while ((ch = fgetc(fp)) != EOF) {
    input[i] = ch;
    i++;
  }
  input[i] = 0;
  inputlen = i;
  
  unsigned int count = 0;
  while (1){
    unsigned char p = 0;
    inckeyword(keyword,KEYWORDLEN);
    if (count%10000==0){
      printf("%s %d\n", keyword, count);
    }
    if (keyword[0] == 'z'+1) {
      break;
    }
    vigenere(input, keyword, output, inputlen, KEYWORDLEN);
    unsigned char numwords = 0;
    for (unsigned char i=0; i<WORDSLEN; i++){
      if (strstr(output,words[i]) == NULL){
        numwords++;
      }
    }
    if (numwords >= THRESHOLD) {
      fprintf(outfp, "%s %s\n", keyword, output);
      printf("%s\n",output);
    }
    count++;
  }
}
void vigenere(char input[], char keyword[], char output[], unsigned int inputlen, unsigned char keywordlen) {
  unsigned char j = 0;
  for (unsigned int i=0; i<inputlen; i++) {
    if ('a' <= input[i] <= 'z') {
      output[i] = (26+input[i]-keyword[j])%26+'a';
    } else {
      output[i] = input[i];
    }
    j++;
    j = j % keywordlen;;
  }
}
void inckeyword(char keyword[], unsigned char keywordlen) {
  keyword[keywordlen-1]++;
  for (unsigned char i=keywordlen-1; i>0; i--) {
    if (keyword[i] == 'z'+1) {
      keyword[i] = 'a';
      keyword[i-1]++;
    }
  }
}
