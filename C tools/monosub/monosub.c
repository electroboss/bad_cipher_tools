#include <stdio.h>
#include <stdlib.h>


char cipher[26] = "bcdefghijklmnopqrstuvwxyza";

int main() {
  FILE *fp;
  char ch;
  char out[20000];

  fp = fopen("input.txt", "r");

  if (fp == NULL) {
    perror("Couldn't find input.txt.\n");
    exit(EXIT_FAILURE);
  }

  unsigned int i = 0;
  while((ch = fgetc(fp)) != EOF) {
    if (ch>='a' && ch <= 'z') {
      out[i] = cipher[ch-'a'];
    } else {
      out[i] = ch;
    }
    i++;
  }
  out[i] = 0;

  printf("%s\n",out);

  return 0;
}
