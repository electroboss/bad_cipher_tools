#include <stdio.h>
#include <stdlib.h>

char code[] = "ohtesrdtiuaerleiihekfpndlhivkehioektewtnnmmatbiecenoeeenhdisyetklelaeeircdeodikctotwhaieneaeaorthigantgasddtfnmowuroeekthwismaoyhuhrixeilroipbestdttabnibitbadiillbaetoelohebhaddsoehwavkaeeliwattytesanygrtatkkosfzctdsuiawchdlntobduhptsnnadalepioinannwyjtrttirnaoqiwutudpldsoiiardnrhrdenvsahbdeunhhakaanhwfdwrhbnabafsioseehenstgahtteetenlhsieaeimsintrtghnnaoknasauyttolrtekptaehfugliciuecsokirooriejebintntaaenhowebdrgantgirmstsoawyughsyedtteanewdlrttttilwrhdsiteeordteneakdosdeietsvetyiesrdtfssdethtliqousaiadfiiipogtndmenbmltnaeuhtotodmaebmseesohtehenebtaoloosoahsattddsieeuliuahateagwertialfaadfseokildthhinrafdwaeorihctmitiusimegllliudiatwlnandascnetlihttwiaetigaaoewmyinoacwaiettotcdtrdayehwbwylanildihthtndaebetudtthtnoexhnslfroytstggaavtehgksetisliedeiuftlnfhtendhnhienhfbitelhaianrtrotlonokwwtceittwthtepccucwowddiysusnotaarotgptidthalvlesnceebbepaihawhekhhsfehshtniulrleodlilnaebtawpensuaieijnotvdarvbukmnnioeegntetedrowihtaoiwovaueeaekaatcarfphwwvaatadaotisoioarudoedeeftinuvmaaeguoagreeteordsuaoldnltidefetniumywnthnoaohdspewcasnacinwctirchrethteenngocnrehalioueotnkeintnaevsbcasahdrencduowmeufgbrtnettsyhoifoaedtoasartbteumisggateotchdhaeatkslodbneeoidnaafneomiehehreotisttgitheebhtonnthspesoneiaetetrhhhdtmodatkhgtermeillkttshecrralrotminmfrenttsobmouoadcadimarrtsdiowaefernentvuycnndaftenoahyeicbyvniurewehnntoiaapndhbenotieteaatynifkaeagaedhxlomtldtstltoeatngotessrezeacgyomnsyihtntyohufinehaussoetsohtlsaomdouteoufgli";
char out[sizeof(code)/sizeof(char)];
void alltransposition(char* code, unsigned int codelen, unsigned char kwlen, void (outfunc)(char*));
void insert(char* outarray, unsigned int outlen, unsigned int index, char inchar);
void incperm(unsigned char* perm, unsigned char permlen);
unsigned long long int factorial(unsigned int x);

int main(){
  FILE *fptr;
  fptr = fopen("out.txt","w");

  void writetofptr(char* string) {
    fprintf(fptr,"%s\n",string);
  }

  alltransposition(code,sizeof(code)/sizeof(char), 9, writetofptr);

  fclose(fptr);

  return 0;
}

void alltransposition(char* code, unsigned int codelen, unsigned char kwlen, void (outfunc)(char*)) { // Last parameter is a function like: void func(char* instr)
  unsigned char perm[kwlen];
  char out[codelen+1];
  for (unsigned int i=0; i<kwlen; i++) {
    perm[i] = 0;
  }
  char *inchunk;
  char *outchunk;
  unsigned long long int kwfac = factorial(kwlen);

  for (unsigned long long int i=0; i<kwfac; i++) {
    for (unsigned int chunk=0; chunk<(codelen/kwlen); chunk++) {
      inchunk = &code[chunk*kwlen];
      outchunk = &out[chunk*kwlen];
      for (unsigned char j=0; j<kwlen; j++) {
        insert(outchunk, kwlen, perm[j], inchunk[j]);
      }
    }
    out[codelen-1] = 0;
    outfunc(out);
    incperm(perm, kwlen);
    if (i%10000 == 0) {
      printf("%d/%lld\n",i,kwfac);
    }
  }
}

void incperm(unsigned char* perm, unsigned char permlen) {
  perm[0]++;
  for (unsigned char i=0; i<permlen; i++) {
    if (perm[i] > i) {
      perm[i+1]++;
      perm[i] = 0;
    }
  }
}

void insert(char* outarray, unsigned int outlen, unsigned int index, char inchar) {
  for (unsigned int i=outlen-1; i>index; i--) {
    outarray[i] = outarray[i-1];
  }
  outarray[index] = inchar;
}

unsigned long long int factorial(unsigned int x) {
  unsigned long long int y = 1;
  for (unsigned int i=1; i<=x; i++) {
    y *= i;
  }
  return y;
}
