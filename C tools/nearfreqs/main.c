#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TO_GENERATE 500000
#define NUM_WORDS 13
#define ROUNDS 50

const char freqcipher[26]="svamntgiudlrpjqhkxyzobewcf";

void randomisecipher(char cipher[26], const char freqcipher[26], const unsigned char times);
int main();

char ciphers[TO_GENERATE][26];
char out[TO_GENERATE][20000];
const char words[NUM_WORDS][5] = {
  "the\0",
  "be\0",
  "to\0",
  "of\0",
  "and\0",
  "a\0",
  "in\0",
  "that\0",
  "have\0",
  "i\0",
  "it\0",
  "for\0",
  "not\0"
};
int seed;

int main(){
  FILE *fp;
  FILE *foutp;

  char ch;
  char p;

  printf("Input a random seed.\n");
  scanf("%ld",&seed);
  srand((unsigned) seed);

  foutp = fopen("out.txt", "w+");


  for (unsigned long q=0; q<ROUNDS; q++){
    printf("Generating ciphers\n");
    for (unsigned int i=0; i<TO_GENERATE; i++) {
      randomisecipher(ciphers[i], freqcipher, 6);
      if (i%100000==0){
        printf("%d\n",i);
      }
    }
    printf("Opening input.txt\n");

    fp = fopen("input.txt", "r");

    if (fp == NULL) {
      perror("Couldn't find input.txt\n");
      exit(EXIT_FAILURE);
    }

    printf("Applying ciphers\n");

    unsigned int i = 0;
    while((ch = fgetc(fp)) != EOF) {
      if (ch>='a' && ch <= 'z') {
        for (unsigned int j=0; j<TO_GENERATE; j++){
          out[j][i] = ciphers[j][ch-'a'];
        }
      }
      i++;
      if (i%100==0) {
        printf("%d\n",i);
      }
    }

    printf("Closing input.txt\n");

    fclose(fp);

    printf("Checking results for words\n");

    for (unsigned int j=0; j<TO_GENERATE; j++){
      out[j][i] = 0;
      p=1;

      for (unsigned char i=0; i<NUM_WORDS; i++){
        if (strstr(out[j],words[i]) == NULL){
          p=0;
          break;
        }
      }

      if (p) {
        printf("Outputting");
        fprintf(foutp,"%s\n",out[j]);
      }
    }

    printf("Done\n");
  }
  printf("Finished\n");
  fclose(foutp);

  return 0;
}

void randomisecipher(char cipher[26], const char freqcipher[26], const unsigned char times) {
  char a;
  for (int i=0; i<26; i++) {
    cipher[i]=freqcipher[i];
  }
  for (int i=0; i<times; i++) {
    for (int j=0; j<25; j++) {
      if (rand()%2>=1) {
        a=cipher[j+1];
        cipher[j+1]=cipher[j];
        cipher[j]=a;
      }
    }
  }
}
