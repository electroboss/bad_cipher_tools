#include <stdio.h>
#include <stdlib.h>


int main() {
  FILE *fp;
  char ch;
  char out[26][10000];

  fp = fopen("input.txt", "r");

  if (fp == NULL) {
    perror("Couldn't find input.txt.\n");
    exit(EXIT_FAILURE);
  }

  unsigned int i = 0;
  while((ch = fgetc(fp)) != EOF) {
    if (ch>='a' && ch <= 'z') {
      for (int j=0; j<26; j++){
        out[j][i]=(ch-'a'+j)%26+'a';
      }
    } else {
      for (int j=0; j<26; j++){
        out[j][i]=ch;
      }
    }
    i++;
  }
  for (int j=0; j<26; j++){
    out[j][i]=0;
  }

  for (int i=0; i<26; i++){
    printf("%s\n", out[i]);
  }

  return 0;
}
